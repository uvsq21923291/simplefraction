package simplefraction;
public class Fraction{
	private int x;
	private int y;
	
	public Fraction(int x, int y) {
		this.x=x;
		this.y=y;
	}
	
	public String toString() {
		return x + "/" + y;
	}
}
